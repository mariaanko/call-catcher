package com.wordpress.mariaanko.callcatcher.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class ServiceReceiver extends BroadcastReceiver {


    TelephonyManager telephony;
    Timer timer = new Timer();

    public void onReceive(Context context, Intent intent) {


        MyPhoneStateListener phoneListener = new MyPhoneStateListener();
        telephony = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        telephony.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
        if(telephony.getCallState()==1){
               //Started the call
               timer.startCall();
        }
        if(telephony.getCallState()==2){
                //received the call
                timer.answered = true;
        }
        if(telephony.getCallState()==0 && timer.answered == false){
            //Idle/stopped the call
            timer.endCall();
            long totalTime = timer.countTime();
            sendNotification(context, Long.toString(totalTime), MyPhoneStateListener.Number);
        }

    }

    public void onDestroy() {
        telephony.listen(null, PhoneStateListener.LISTEN_NONE);
    }

    public static void sendNotification(Context ctx, String totalTime, String incomingNumber){
        Notification.Builder builder;

        String Number = getContactDisplayNameByNumber(ctx, incomingNumber);
        if(Number=="?"){
            Number = incomingNumber;
        }
        builder = new Notification.Builder(ctx)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(Number)
                .setContentText(NotificationData.message + " " + totalTime + " " + NotificationData.messageEnd);

        Intent notificationIntent = new Intent(ctx, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        // Add as notification
        NotificationManager manager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        if(incomingNumber!=null){

            manager.notify(0, builder.build());
        }
    }

    public static String getContactDisplayNameByNumber(Context ctx, String number) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        String name = "?";

        ContentResolver contentResolver = ctx.getContentResolver();
        Cursor contactLookup = contentResolver.query(uri, new String[] {BaseColumns._ID,
                ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);

        try {
            if (contactLookup != null && contactLookup.getCount() > 0) {
                contactLookup.moveToNext();
                name = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
                //String contactId = contactLookup.getString(contactLookup.getColumnIndex(BaseColumns._ID));
            }
        } finally {
            if (contactLookup != null) {
                contactLookup.close();
            }
        }

        return name;
    }
}


class Timer {

    public static long callStart;
    public static long callEnd;
    public static boolean answered = false;
    public void startCall(){

        callStart = System.currentTimeMillis();

    }

    public void endCall(){

        callEnd = System.currentTimeMillis();

    }

    public long countTime(){

        long elapsed = callEnd - callStart;
        return elapsed / 1000;

    }
}

class NotificationData{

    public static String message = "Ring-through took";
    public static String messageEnd = "seconds";


}