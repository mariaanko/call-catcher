package com.wordpress.mariaanko.callcatcher.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CallLog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText messageEdit = (EditText) findViewById(R.id.editMessage);
        EditText messageEditEnd = (EditText) findViewById(R.id.editMessageEnd);

        messageEdit.setText(NotificationData.message);
        messageEditEnd.setText(NotificationData.messageEnd);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void saveMessage(View view){

        EditText messageEdit = (EditText) findViewById(R.id.editMessage);
        EditText messageEditEnd = (EditText) findViewById(R.id.editMessageEnd);
        String message = messageEdit.getText().toString();
        String messageEnd = messageEditEnd.getText().toString();
        NotificationData.message = message;
        NotificationData.messageEnd = messageEnd;

        CharSequence text = "Message saved!";
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(this, text, duration);
        toast.show();

        this.finish();

    }

    public void startCallLog(View view){

        Intent intent = new Intent(this, callLog.class);
        startActivity(intent);

    }
}
